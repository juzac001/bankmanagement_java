## Bankmanagement

# Kurze Erklärung
Hierbei handelt es sich um kleines CLI "Bankmanagement" System, bei dem mit Hilfe einer SQLite Datenbank Kontos erstellt werden können, der Kontostand abgefragt werden kann, und Beträge abgehoben oder eingezahlt werden können.
Das Konto kann "überzogen" werden, es werden also auch negative Beträge dargestellt. 

Wichtig: die SQLite Datenbank muss bereits vorhanden sein (oder über die beiliegende SQLite3 Datei erstellt werden), sie kann nicht innerhalb des Programms erstellt werden.
Die von mir vorher erstellte Datenbank "dbV1_0.db" enthält einen Table "kunden", in den Einträge mit den Werten "vorname", "nachname" und "kontostand" (als float) hinzugefügt werden können, darauf ist das Programm ausgelegt. Würden weitere Spalten hinzugefügt oder die Namensgebung verändert, kann das die Funktionalität stark beeinträchtigen.

Weitere Informationen können der JavaDoc innerhalb der Main Datei entnommen werden.

Das Programm wurde unter Linux (Fedora 39) mit Java 21 erstellt und getestet. 
Die beiden externen Libraries für die SQLite Datenbank sind im Ordner "libs" enthalten. 
