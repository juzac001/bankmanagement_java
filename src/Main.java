import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Scanner;

public class Main {
    //Benötigte Variablen
    static String dbPath = "libs/dbV1_0.db";
    static String jdbcUrl = "jdbc:sqlite:" + dbPath;
    static String testabfrage = " SELECT * FROM kunden;";
    static String vorname, nachname, prompt;

    //Benötigte Instanzen
    static Main db;
    static Scanner input;
    StringBuilder result;
    ResultSet resultSet = null;
    Path path = Paths.get(dbPath);
    Connection connection;
    Statement statement;

    /**
     * Wenn die Datenbank vorhanden ist, wird eine Verbindung aufgebaut, andernfalls wird eine Nachricht auf der
     * Konsole ausgegeben und das Programm beendet.
     */
    public void connect() {
        if (Files.exists(path)) {
            try {
                connection = DriverManager.getConnection(jdbcUrl);
                statement = connection.createStatement();
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            System.out.println("Fehler: Datenbank nicht gefunden. Programm wird beendet");
            System.exit(0);
        }
    }

    /**
     * Die Datenbank wird nach einem bestimmten Eintrag durchsucht.
     *
     * @param vorname
     * @param nachname
     * @return Falls ein Eintrag gefunden wurde, wird dieser zurückgegeben, ansonsten "Nicht vorhanden."
     */
    public String search(String vorname, String nachname) {
        prompt = "SELECT * FROM kunden WHERE nachname='" + nachname + "' AND vorname='" + vorname + "';";
        result = new StringBuilder();
        try {
            resultSet = statement.executeQuery(prompt);
            while (resultSet.next()) {
                result.append(resultSet.getString("nachname")).append(" ").append(resultSet.getString("vorname"));
            }
            if (!result.isEmpty()) {
                return result.toString();
            } else return "Nicht vorhanden";
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Checkt, ob ein Eintrag vorhanden ist.
     *
     * @param vorname
     * @param nachname
     * @return true/false
     */
    public Boolean bereitsVorhanden(String vorname, String nachname) {
        if (db.search(vorname, nachname).compareTo("Nicht vorhanden") == 0) {
            return false;
        } else return true;
    }

    /**
     * Zu Testzwecken um alle Datenbankeinträge auszugeben.
     */
    public void selectAll() {
        try {
            resultSet = statement.executeQuery(testabfrage);
            while (resultSet.next()) {
                System.out.println(resultSet.getString("nachname") + " " + resultSet.getString("vorname") + " " + resultSet.getString("kontostand"));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Fügt einen Eintrag der Datenbank hinzu, wenn dieser noch nicht vorhanden ist.
     *
     * @param vorname
     * @param nachname
     */
    public void insert(String vorname, String nachname) {
        if (!bereitsVorhanden(vorname, nachname)) {
            prompt = "INSERT INTO kunden VALUES('" + vorname + "','" + nachname + "','" + "0.0" + "');";
            try {
                statement.executeUpdate(prompt);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("Bereits vorhanden.");
        }
    }

    /**
     * Entfernt einen Eintrag, falls dieser vorhanden ist.
     *
     * @param vorname
     * @param nachname
     */
    public void remove(String vorname, String nachname) {
        if (bereitsVorhanden(vorname, nachname)) {
            prompt = "DELETE FROM kunden WHERE nachname='" + nachname + "' AND vorname='" + vorname + "';";
            try {
                System.out.println(prompt);
                statement.executeUpdate(prompt);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("Eintrag nicht vorhanden.");
        }
    }

    /**
     * Gibt den gesuchten Wert (zum Beispiel den Kontostand) an.
     *
     * @param vorname
     * @param nachname
     * @param wert
     * @return String wert
     */
    public String getValue(String vorname, String nachname, String wert) {
        prompt = "SELECT " + wert + " FROM kunden WHERE nachname='" + nachname + "' AND vorname='" + vorname + "';";
        try {
            resultSet = statement.executeQuery(prompt);
            return resultSet.getString(wert);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Zeigt das Menü, von dem aus man die verschiedene Aktionen durchführen kann.
     */
    public void menu() {
        System.out.println("""
                Wählen Sie eine Option:
                a) Konto eröffnen
                b) Kontostand
                c) Geld abheben
                d) Geld einzahlen
                e) Beenden
                """);

        System.out.print("Ihre Eingabe: ");
        String auswahl = input.nextLine();
        switch (auswahl) {
            case "a" -> db.kontoEroeffnen();
            case "b" -> db.kontostand();
            case "c" -> db.abheben();
            case "d" -> db.einzahlen();
            case "e" -> System.exit(0);
        }
    }

    /**
     * Prüft, ob die Nutzerdaten bereits vorhanden sind, wenn nicht, wird über die insert-Methode ein neuer
     * Eintrag zur Datenbank hinzugefügt.
     */
    public void kontoEroeffnen() {
        System.out.print("Bitte geben Sie nun ihre Daten ein." + "\n" + "Vorname: ");
        vorname = input.nextLine().trim();
        System.out.print("Nachname: ");
        nachname = input.nextLine().trim();

        if (!bereitsVorhanden(vorname, nachname)) {
            db.insert(vorname, nachname);
        } else {
            System.out.println("Konto bereits vorhanden");
        }
    }

    /**
     * Prüft, ob die Nutzerdaten bereits vorhanden sind, wenn ja, wird der Kontostand über die getValue-Methode
     * ausgegeben.
     */
    public void kontostand() {
        System.out.print(" Bitte geben Sie nun ihre Daten ein." + "\n" + "Vorname: ");
        vorname = input.nextLine().trim();
        System.out.print("Nachname: ");
        nachname = input.nextLine().trim();

        if (bereitsVorhanden(vorname, nachname)) {
            System.out.println("Ihr Kontostand beträgt " + db.getValue(vorname, nachname, "kontostand") + " Euro.");
        } else {
            System.out.println("Konto nicht vorhanden");
        }
    }

    /**
     * Konvertiert den Stringwert des Betrags der abgehoben werden soll in einen float-Wert und prüft, ob die Nutzerdaten
     * bereits vorhanden sind. Wenn ja, wird der alte Kontostand abzüglich des abgehobenen Betrags in die Datenbank
     * übertragen.
     */
    public void abheben() {
        System.out.print(" Bitte geben Sie nun ihre Daten ein." + "\n" + "Vorname: ");
        vorname = input.nextLine().trim();
        System.out.print("Nachname: ");
        nachname = input.nextLine().trim();
        System.out.print("Wie viel möchten Sie abheben?");

        float betrag = Float.parseFloat(getValue(vorname, nachname, "kontostand")) - Float.parseFloat(input.nextLine());

        if (bereitsVorhanden(vorname, nachname)) {
            prompt = "UPDATE kunden SET kontostand = " + betrag + " WHERE nachname ='" + nachname + "' AND vorname='" + vorname + "';";
            try {
                statement.executeUpdate(prompt);
                System.out.println("Ihr neuer Kontostand beträgt: " + betrag + " Euro.");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("Konto nicht vorhanden.");
        }
    }

    /**
     * Konvertiert den Stringwert des Betrags der eingezahlt werden soll in einen float-Wert und prüft, ob die Nutzerdaten
     * bereits vorhanden sind. Wenn ja, wird der alte Kontostand plus den eingezahlten Betrag in die Datenbank
     * übertragen.
     */
    public void einzahlen() {
        System.out.print("Bitte geben Sie nun ihre Daten ein." + "\n" + "Vorname: ");
        vorname = input.nextLine().trim();
        System.out.print("Nachname: ");
        nachname = input.nextLine().trim();
        System.out.print("Wie viel möchten Sie einzahlen?");

        float betrag = Float.parseFloat(getValue(vorname, nachname, "kontostand")) + Float.parseFloat(input.nextLine());

        if (bereitsVorhanden(vorname, nachname)) {
            prompt = "UPDATE kunden SET kontostand = " + betrag + " WHERE nachname ='" + nachname + "' AND vorname='" + vorname + "';";
            try {
                statement.executeUpdate(prompt);
                System.out.println("Ihr neuer Kontostand beträgt: " + betrag + " Euro.");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("Konto nicht vorhanden.");
        }
    }

    /**
     * Erzeugt ein Objekt, an dem die Methoden aufgerufen werden können. Erzeugt einen Scanner für die Nutzereingaben.
     * Verbindet die Datenbank und ruft das Menü auf.
     */
    public static void main(String[] args) {
        db = new Main();
        input = new Scanner(System.in);
        db.connect();
        while (true) {
            db.menu();
            System.out.println();
        }

    }
}